Paga+Tarde (https://www.pagamastarde.com/ ) module allows your DrupalCommerce
shop to offer frinancing payments.


Configuration
-------------

This payment module may be added and configured via the normal Rules interface.


Paga+Tarde account settings
------------------------------

Once installed you only have to fill the account details you have in the
pagamastarde.com backoffice.
For more information read the Guide.pdf provided with the module.


Troubleshooting
---------------

For any problems, doubts or questions, please contact us at soporte@pagamastarde.com 

Credits
-------

This module is based on a Authorize.net SIM-only implementation and implemented by 
Albert Fatsini from pagamastarde.com team.
